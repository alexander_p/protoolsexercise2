#Meeting
=========

20.06.2014

### Zusammenfassung
* Topic
 * Überarbeitung des Backtracking Beispiels
* Uhrzeit
 * 16-16.30 Uhr
* Ort
 * HQ Klam
* Verfasser des Protokolls
 * Palmanshofer Alexander

### Agenda
* Beschreibung des Problems
* Diskussion über Lösungsmöglichkeiten

### Teilnehmer
1. Palmanshofer Alexander
2. Imaginäre Person 1
3. Imaginäre Person 2

### Notizen
Die derzeitige Implementation von Backtracking ist nicht zufriedenstellend. Es wird vorgeschlagen, dass das Beispiel überarbeitet wird, sodass Labyrinthe mit einem Backtracking Algorithmus durchlaufen werden können.
package algodat.tests;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import algodat.exercise01.Quicksort;
import algodat.exercise01.Sorter;

public class QuicksortTests {

	@Rule
	public ExpectedException expected = ExpectedException.none();
	
	@Test
	public void testNulls() throws Exception {
		Integer[] a = new Integer[10];
		Sorter<Integer> intSort = new Quicksort<Integer>();

		// Null Pointer
		Integer[] nullPtr = null;
		intSort.sort(nullPtr);
		
		// Empty Array
		Integer[] emptyArray = new Integer[0];
		intSort.sort(emptyArray);
		
		// Null Array
		expected.expect(Exception.class);
		expected.expectMessage("Please strip all nulls from your array before passing it to the sort method");
		intSort.sort(a);
	}

	@Test
	public void testArrays() throws Exception {
		Integer[] a = new Integer[10];
		Sorter<Integer> intSort = new Quicksort<Integer>();
		
		// Reverse sorted Integer Array
		a = new Integer[10];
		for (int i = 0; i < a.length; i++)
			a[i] = new Integer(9 - i);
		intSort.sort(a);
		assertArrayEquals(new Integer[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, a);
		
		// String Array
		String[] a2 = {"Hans", "Brandy", "Stefan", "Mayr", "Palmi"};
		String[] a2Sorted = a2.clone();
		Arrays.sort(a2Sorted);
		
		Sorter<String> stringSort = new Quicksort<String>();
		stringSort.sort(a2);
		assertArrayEquals(a2Sorted, a2);
		
		// Array with equal values
		String[] a3 = {"AAA", "AAA", "AAA", "AAA"};
		String[] a3Sorted = a3.clone();
		Arrays.sort(a3Sorted);
		
		stringSort.sort(a2);
		assertArrayEquals(a2Sorted, a2);
	}
}

package algodat.tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Random;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import algodat.exercise01.Quicksort;
import algodat.exercise01.Sorter;
import algodat.exercise02.Mergesort;

public class MergesortTests {

	@Rule
	public ExpectedException expected = ExpectedException.none();
	private Integer[] hundredInts;
	private Integer[] thousandInts;
	private Integer[] tenthousandInts;
	private String[] hundredStrings;
	private String[] thousandStrings;
	private String[] tenthousandStrings;

	@Test
	public void testNulls() throws Exception {
		Integer[] a = new Integer[10];
		Sorter<Integer> intSort = new Mergesort<Integer>();

		// Null Pointer
		Integer[] nullPtr = null;
		intSort.sort(nullPtr);

		// Empty Array
		Integer[] emptyArray = new Integer[0];
		intSort.sort(emptyArray);

		// Null Array
		expected.expect(Exception.class);
		expected.expectMessage("Please strip all nulls from your array before passing it to the sort method");
		intSort.sort(a);
	}

	@Test
	public void testArrays() throws Exception {
		Integer[] a = new Integer[10];
		Sorter<Integer> intSort = new Mergesort<Integer>();

		// Reverse sorted Integer Array
		a = new Integer[10];
		for (int i = 0; i < a.length; i++)
			a[i] = new Integer(9 - i);
		intSort.sort(a);
		assertArrayEquals(new Integer[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, a);

		// String Array
		String[] a2 = { "Hans", "Brandy", "Stefan", "Mayr", "Palmi" };
		String[] a2Sorted = a2.clone();
		Arrays.sort(a2Sorted);

		Sorter<String> stringSort = new Quicksort<String>();
		stringSort.sort(a2);
		assertArrayEquals(a2Sorted, a2);

		// Array with equal values
		String[] a3 = { "AAA", "AAA", "AAA", "AAA" };
		String[] a3Sorted = a3.clone();
		Arrays.sort(a3Sorted);

		stringSort.sort(a2);
		assertArrayEquals(a2Sorted, a2);
	}

	public void initArrays(boolean intsOrStrs) {
		Random random = new Random();
		if(intsOrStrs){
			hundredInts = new Integer[100];
			thousandInts = new Integer[1000];
			tenthousandInts = new Integer[10000];

			for (int i = 0; i < hundredInts.length; i++) {
				hundredInts[i] = random.nextInt();
			}
			for (int i = 0; i < thousandInts.length; i++) {
				thousandInts[i] = random.nextInt();
			}
			for (int i = 0; i < tenthousandInts.length; i++) {
				tenthousandInts[i] = random.nextInt();
			}			
		} else {
			hundredStrings = new String[100];
			thousandStrings = new String[1000];
			tenthousandStrings = new String[10000];
			
			for (int i = 0; i < hundredStrings.length; i++) {
				StringBuilder builder = new StringBuilder();
				for (int j = 0; j < 100; j++) {
					builder.append((char) (random.nextInt(25) + 65));
				}
				hundredStrings[i] = builder.toString();
			}
			for (int i = 0; i < thousandStrings.length; i++) {
				StringBuilder builder = new StringBuilder();
				for (int j = 0; j < 100; j++) {
					builder.append((char) (random.nextInt(25) + 65));
				}
				thousandStrings[i] = builder.toString();
			}
			for (int i = 0; i < tenthousandStrings.length; i++) {
				StringBuilder builder = new StringBuilder();
				for (int j = 0; j < 100; j++) {
					builder.append((char) (random.nextInt(25) + 65));
				}
				tenthousandStrings[i] = builder.toString();
			}
		}
	}

	@Test
	public void testPerformance() throws Exception {
		Sorter<Integer> qsInt = new Quicksort<>();
		Sorter<String> qsStr = new Quicksort<>();
		Sorter<Integer> msInt = new Mergesort<>();
		Sorter<String> msStr = new Mergesort<>();

		long start, time;

		// Ints

		initArrays(true);

		start = System.currentTimeMillis();
		qsInt.sort(hundredInts);
		time = System.currentTimeMillis() - start;
		System.out.println("QS 100 Int: " + time + " ms");

		start = System.currentTimeMillis();
		qsInt.sort(thousandInts);
		time = System.currentTimeMillis() - start;
		System.out.println("QS 1k Int: " + time + " ms");

		start = System.currentTimeMillis();
		qsInt.sort(tenthousandInts);
		time = System.currentTimeMillis() - start;
		System.out.println("QS 10k Int: " + time + " ms");

		initArrays(true);

		start = System.currentTimeMillis();
		msInt.sort(hundredInts);
		time = System.currentTimeMillis() - start;
		System.out.println("MS 100 Int: " + time + " ms");

		start = System.currentTimeMillis();
		msInt.sort(thousandInts);
		time = System.currentTimeMillis() - start;
		System.out.println("MS 1k Int: " + time + " ms");

		start = System.currentTimeMillis();
		msInt.sort(tenthousandInts);
		time = System.currentTimeMillis() - start;
		System.out.println("MS 10k Int: " + time + " ms");

		// Strings

		initArrays(false);

		start = System.currentTimeMillis();
		qsStr.sort(hundredStrings);
		time = System.currentTimeMillis() - start;
		System.out.println("QS 100 Str: " + time + " ms");

		start = System.currentTimeMillis();
		qsStr.sort(thousandStrings);
		time = System.currentTimeMillis() - start;
		System.out.println("QS 1k Str: " + time + " ms");

		start = System.currentTimeMillis();
		qsStr.sort(tenthousandStrings);
		time = System.currentTimeMillis() - start;
		System.out.println("QS 10k Str: " + time + " ms");

		initArrays(false);

		start = System.currentTimeMillis();
		msStr.sort(hundredStrings);
		time = System.currentTimeMillis() - start;
		System.out.println("MS 100 Str: " + time + " ms");

		start = System.currentTimeMillis();
		msStr.sort(thousandStrings);
		time = System.currentTimeMillis() - start;
		System.out.println("MS 1k Str: " + time + " ms");

		start = System.currentTimeMillis();
		msStr.sort(tenthousandStrings);
		time = System.currentTimeMillis() - start;
		System.out.println("MS 10k Str: " + time + " ms");
	}
}

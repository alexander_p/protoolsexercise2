package algodat.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import algodat.exercise03.Backtracking;

public class BacktrackingTests {

	@Test
	public void test() {
		Backtracking bt = new Backtracking();
		
		bt.printAllValid();
		// This is confirmed for validity by human eyes using the solution in Exercise 03
	}

}

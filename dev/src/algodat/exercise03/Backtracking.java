package algodat.exercise03;

public class Backtracking {
	public enum Person {
		anton, berta, clemens, doris
	};

	/* visitors[x] = true --> x will attend Ossis party */
	private boolean[] visitors = new boolean[Person.values().length];
	
	/***
	 * Checks whether the given combination of visitors is possible according to
	 * the given constraints.
	 * 
	 * @param visitors
	 *            Boolean array of visitors, indexed by ordinal values of Person
	 * @return Boolean
	 */
	public boolean isValid(boolean[] visitors) {
		/*
		 * check all conditions and return true if visitors holds a valid
		 * combination
		 */
		// At least one person must visit Ossi
		if (!visitors[Person.anton.ordinal()]
				&& !visitors[Person.berta.ordinal()]
				&& !visitors[Person.clemens.ordinal()]
				&& !visitors[Person.doris.ordinal()])
			return false;
		// Anton and Doris can't visit the party together
		if (visitors[Person.anton.ordinal()]
				&& visitors[Person.doris.ordinal()])
			return false;
		// If Berta is attending, Clemens has to go as well
		if (visitors[Person.berta.ordinal()]
				&& !visitors[Person.clemens.ordinal()])
			return false;
		// When Anton and Clemens are going, Berta has to stay at home
		if (visitors[Person.anton.ordinal()]
				&& visitors[Person.clemens.ordinal()]
				&& visitors[Person.berta.ordinal()])
			return false;
		// Should Anton stay at home, only Clemens or Doris can go (exklusive)
		if (!visitors[Person.anton.ordinal()])
			if (visitors[Person.clemens.ordinal()]
					&& visitors[Person.doris.ordinal()])
				return false;
		
		return true;
	}

	/** Set the first valid state for the visitors[] array */
	public void setStartState() {
		for(int i = 0; i < visitors.length; i++) {
			visitors[i] = false;
		}
	}

	/**
	 * Based on the current state of the visitors[] array, set the next valid
	 * state.
	 *
	 * @param level
	 *            determines the start position in the visitors[] array from
	 *            which to change values.
	 * @return true if the new state is valid, false if no new state could be
	 *         found for this level.
	 */
	public boolean nextState(int level) {
		// Return value used to determine whether we should flip booleans further
		// up the combinations "tree"
		boolean ret = false;
		// Traverse to the "leaf" of the combinations "tree"
		if(level < visitors.length - 1){
			ret = nextState(level + 1);
		}
		// Should the return value be false...
		if(!ret){
			// ... and the value also be false
			if(!visitors[level]) {
				// Flip the boolean and return true, which means that no further
				// changes should be done, because we already changed something
				visitors[level] = !visitors[level];
				return true;
			}else{
				// If the value is true, also flip it, but return false.
				// This means that we want to change something further up the
				// combinations "tree"
				// ==> Backtracking
				visitors[level] = !visitors[level];
				return false;
			}
		}else{
			// Propagate the true back up the call chain, don't change anything
			return true;
		}
	}

	public void printAllValid() {
		setStartState();
		do {
			// evaluate candidate
			if (isValid(visitors)) {
				/* print */
				System.out.print(visitors[Person.anton.ordinal()] ? "Anton "
						: "Leer ");
				System.out.print(visitors[Person.berta.ordinal()] ? "Berta "
						: "Leer ");
				System.out
						.print(visitors[Person.clemens.ordinal()] ? "Clemens "
								: "Leer ");
				System.out.print(visitors[Person.doris.ordinal()] ? "Doris "
						: "Leer ");
				System.out.println();

			}
		} while (nextState(0));
	}
}
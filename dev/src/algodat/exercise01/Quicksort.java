package algodat.exercise01;

public class Quicksort<T> implements Sorter<T> {
	
	/**
	 * Sorts the given array of Comparables.
	 * Does nothing when given a null pointer.
	 * @param a Array of Comparables
	 * @throws Exception when there are null values inside the array
	 */
	public void sort(Comparable<T>[] a) throws Exception{
		if (a != null)
			qsort(checkForNulls(a), 0, a.length - 1);
	}
	
	/**
	 * Checks for null values inside the array and throws an Exception when that's the case.
	 * @param a Array of Comparables
	 * @return The pointer to the previously given array
	 * @throws Exception when there are null values inside the array
	 */
	private Comparable<T>[] checkForNulls(Comparable<T>[] a) throws Exception{
		for(Comparable<T> element : a){
			if(element == null)
				throw new Exception("Please strip all nulls from your array before passing it to the sort method");
		}
		return a;
	}

	/**
	 * Implementation of the recursive quicksort algorithm. It handles all objects that correctly implement
	 * the Comparable interface.
	 * @param a Array of Comparables
	 * @param from Start index, starts with 0
	 * @param to End index, starts with last index in array
	 */
	private void qsort(Comparable<T>[] a, int from, int to) {
		// x is the pivot element
		Comparable<T> x;
		// i and j are the current running indices
		int i, j;
		// Only run when the indices make sense
		if (from < to) {
			// Choose the pivot element as the element in the middle of the array
			x = a[(from + to) / 2];
			i = from;
			j = to;
			do {
				// Advance the from index when the left element is smaller than the pivot
				while (a[i].compareTo((T) x) < 0)
					i++;
				// Advance the to index when the pivot element is smaller than the right element
				while (x.compareTo((T) a[j]) < 0)
					j--;
				// Swap values when there are elements eligible for swapping
				if (i <= j) {
					Comparable<T> tmp;
					tmp = a[i];
					a[i] = a[j];
					a[j] = tmp;

					i++;
					j--;
				}
			} while (i < j);
			// Recursively call quicksort again for the part left of pivot
			qsort(a, from, j);
			// ... and the part right
			qsort(a, i, to);
		}
	}
}
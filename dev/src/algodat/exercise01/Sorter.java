package algodat.exercise01;

public interface Sorter<T> {
    public void sort(Comparable<T>[] a) throws Exception;
}
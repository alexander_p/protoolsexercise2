package algodat.exercise02;

import algodat.exercise01.Sorter;

public class Mergesort<T> implements Sorter<T> {
	
	/**
	 * Sorts the given array of Comparables.
	 * Does nothing when given a null pointer.
	 * @param a Array of Comparables
	 * @throws Exception when there are null values inside the array
	 */
	@Override
	public void sort(Comparable<T>[] a) throws Exception {
		if (a != null) {
			Comparable<T>[] tmp = new Comparable[a.length];
			msort(checkForNulls(a), tmp, 0, a.length-1);
		}
	}
	
	/**
	 * Checks for null values inside the array and throws an Exception when that's the case.
	 * @param a Array of Comparables
	 * @return The pointer to the previously given array
	 * @throws Exception when there are null values inside the array
	 */
	private Comparable<T>[] checkForNulls(Comparable<T>[] a) throws Exception{
		for(Comparable<T> element : a){
			if(element == null)
				throw new Exception("Please strip all nulls from your array before passing it to the sort method");
		}
		return a;
	}

	/**
	 * Implementation of the recursive mergesort algorithm. It handles all objects that correctly implement
	 * the Comparable interface.
	 * @param a Array of Comparables
	 * @param tmp Temporary Array used for merging
	 * @param from Start index, starts with 0
	 * @param to End index, starts with last index in array
	 */
	private void msort(Comparable<T>[] a, Comparable<T>[] tmp, int from, int to) {
		// Only run when the indices make sense
		if (from < to) {
			// Choose the pivot index as the middle element of the array
			int pivotIdx = (from + to) / 2;
			// Sort the part left of the pivot index
			msort(a, tmp, from, pivotIdx);
			// Sort the part right of the pivot index
			msort(a, tmp, pivotIdx + 1, to);
			// Merge left and right
			merge(a, tmp, from, pivotIdx + 1, to);
		}
	}

	/**
	 * Merges the left and right parts of the pivot in the array.
	 * @param a Array of Comparables that will be sorted
	 * @param tmp Temporary Array used for merging
	 * @param fromLeft Start index left
	 * @param fromRight End index left
	 * @param toRight End index right
	 */
	private void merge(Comparable<T>[] a, Comparable<T>[] tmp, int fromLeft, int fromRight, int toRight) {
		// "Infer" the pivot index
		int pivotIdx = fromRight - 1;
		// Temporary running index starting with fromLeft
		int k = fromLeft;
		int num = toRight - fromLeft + 1;

		// Run as long as the indices are within array range
		while (fromLeft <= pivotIdx && fromRight <= toRight)
			// Either advance left index or right index depending on what value is smaller
			if (a[fromLeft].compareTo((T) a[fromRight]) <= 0)
				tmp[k++] = a[fromLeft++];
			else
				tmp[k++] = a[fromRight++];

		// Copy from left part
		while (fromLeft <= pivotIdx)
			tmp[k++] = a[fromLeft++];

		// Copy from right part
		while (fromRight <= toRight)
			tmp[k++] = a[fromRight++];

		// Merge back the temporary array into the given "to-sort" array
		for (int i = 0; i < num; i++, toRight--)
			a[toRight] = tmp[toRight];
	}
}